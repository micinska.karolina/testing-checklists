## CHECKLIST - finalizacja zamówienia - sklep internetowy

- [ ] 1. Mogę złożyć zamówienie jako zalogowany użytkownik  
- [ ] 2. Mogę złożyć zamówienie jako niezalogowany użytkownik  
- [ ] 3. Mam różne możliwości płatności (przelew, paypal, szybkie płatności)
- [ ] 4. Przekierowanie na stronę banku
- [ ] 5. Strona potwierdzająca złożenie zamówienia
- [ ] 6. Potwierdzenie wysłane na maila (z numerem zamówienia)
- [ ] 7. Potwierdzenie jako wyświetlający się komunikat 
- [ ] 8. Status zamówienia
