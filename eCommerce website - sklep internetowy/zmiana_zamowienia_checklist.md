## CHECKLIST - zmiana zamówienia - sklep internetowy  

- [ ] 1. Mogę edytować zamówienie  
- [ ] 2. Mogę anulować zamówienie  
- [ ] 3. Mogę zmienić sposób przesyłki  
- [ ] 4. Mogę śledzić przesyłkę  
- [ ] 5. Mogę dokonać zwrotu zamówienia