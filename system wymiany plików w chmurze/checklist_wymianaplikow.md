## CHECKLIST - wymiana plików w chmurze  
###### np. Google Drive

- [ ] 1. Mogę się zalogować  
- [ ] 2. Mogę dodać plik do dysku  
- [ ] 3. Mogę usunąć plik z dysku  
- [ ] 4. Mogę edytować pliki w dysku
- [ ] 5. Mogę udostępnić plik z dysku  
- [ ] 6. Mogę dodać uprawnionego użytkownika  
- [ ] 7. Mogę sortować pliki według daty dodania  
- [ ] 8. Mogę sortować pliki według rozmiaru  
- [ ] 9. Mogę sortować pliki alfabetycznie  
- [ ] 10. Mogę tworzyć foldery plików  
- [ ] 11. Mogę tworzyć kopie zapasowe plików  
- [ ] 12. Mogę oznaczać gwiazdką pliki  
- [ ] 13. Mogę wyszukać pliki na dysku  
- [ ] 14. Mogę zwiększyć ilość miejsca na dane na dysku  
- [ ] 15. Mogę się wylogować  