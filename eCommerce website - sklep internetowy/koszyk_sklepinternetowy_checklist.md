## CHECKLIST - koszyk - sklep internetowy:  

- [ ] 1. Mogę dodać produkt do koszyka  
- [ ] 2. Mogę obejrzeć zawartość koszyka  
- [ ] 3. Mogę usunąć przedmiot z koszyka  
- [ ] 4. Mogę złożyć zamówienie  
- [ ] 5. Mogę dodać do koszyka więcej niż jeden przedmiot  
- [ ] 6. Mogę dodać do koszyka więcej niż jedną sztukę tego samego produktu  
- [ ] 7. Mogę zmieniać ilość przedmiotów w koszyku  
- [ ] 8. Nie mogę złożyć zamówienia mając pusty koszyk  
- [ ] 9. Mogę dodać do koszyka przedmiot i kontynuować zakupy  
- [ ] 10. Koszyk nalicza poprawną liczbę produktów  
- [ ] 11. Koszyk nalicza poprawną cenę poszczególnych produktów  
- [ ] 12. Koszyk nalicza poprawną sumę produktów  
- [ ] 13. Zawartość koszyka zostaje zmieniana - koszyk aktualizuje się w czasie rzeczywistym  
- [ ] 14. Mogę edytować produkt w koszyku - ilość, kolor, rozmiar i in.  
- [ ] 15. Koszyk przyjmuje bony rabatowe  
- [ ] 16. Koszyk poprawnie nalicza rabat  
- [ ] 17. Mogę sprawdzić/wybrać koszty dostawy  
- [ ] 18. Zawartość koszyka pozostaje w nim mimo zamknięcia karty/przeglądarki  